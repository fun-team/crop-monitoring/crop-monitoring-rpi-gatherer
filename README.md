# Rpi-gatherer for crop-monitoring

This repository contains some file to create a MQTT gatherer used with the crop monitoring station.

In this project, the RaspberryPi 3B+ acts as a data gatherer. To do so, there is multiple parts implemented on it :

- DNSMasq/HostAPD : These services are used to open an wifi acces point on the RPi on which each ESP32 will automatically connect itself on boot.
- Eclipse Mosquitto : This is the open source broker used to implement the MQTT protocol
- SQLite3 : This common and light database is used to store each data gathered in data tables. There is one table for each ESP32 node.
- mqtt_gatherer.py : This simple python script is meant to be executed once in background. It listens for new data on each ESP32 topic of the MQTT-broker, format it, then store it in the database.

We will now give a few more details on each part.

## DNSMasq/HostAPD

These services allow us to create an access point on which each ESP32 will connect to send data over the MQTT-Broker. When starting the RPi, you might have to restart each service :

> sudo systemctl restart dnsmasq
> sudo systemctl restart hostapd

Apart from that, there is nothing much to do.

It creates a network named esp32-gath, free of authentification methods, and with no internet access. Once the RPi will be fully started, you may use this access point to connect your computer to it, and observe the flow of data on the MQTT-broker.

## Eclipse Mosquitto

We used the last version of Mosquitto. You can observe the flow of messages over the mqtt broker by using a free software such as [MQTT Explorer](http://mqtt-explorer.com/). Right now there is no authentification method to acces the broker.

When an ESP32 sends data to the broker, it will be shown under a topic named by it's MAC address. For each topic, there are 4 subtopics : Temperature, Brightness, Humidity and Dew Point, each one containing the corresponding raw data from the ESP sensor.

After starting the RPi, you can check that the Mosquitto broker has started using the following command :

> sudo systemctl status mosquitto

If a problem has occured, the following command should make it work :

> sudo systemctl restart mosquitto

## SQLite3 :

SQLite3 has been used to create a database in which every data received on the MQTT-broker is stored. In this database, there are 5 tables created : one to store the test data, and four to store the actual datas.

In case you need to add more ESP32 to the network, you have to manually create a new table using SQL commands.

## mqtt_gatherer.py

In order to make this python script work, you have to activate the corresponding virtual environment :

> cd ./Documents/crop-monitoring-rpi-gatherer

When in the ~/Documents/crop-monitoring-rpi-gatherer file :

> source ./gathering/bin/activate

Then you can start the python script in background :

> python mqtt-gatherer.py &

## What's next ?

If you followed the previous step, you should have a working platform that will gather datas received from the ESP32 sensors. It's up to you to develop a methods that fits your needs to extract the datas from the database.
