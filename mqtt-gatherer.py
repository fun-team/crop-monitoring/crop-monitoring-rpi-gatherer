import paho.mqtt.client as mqtt
import time
import json
import sqlite3
import datetime

# Define Variables
MQTT_BROKER = "192.168.0.1"
MQTT_PORT = 1883
MQTT_TOPIC = [("68:67:25:50:05:88/temperature",0),
		("58:CF:79:D9:65:F8/temperature",0),
		("68:67:25:4F:37:18/temperature",0),
		("68:67:25:4D:9C:E8/temperature",0),
		("58:CF:79:D9:BE:34/temperature",0),
		("68:67:25:50:05:88/humidity",0),
		("58:CF:79:D9:65:F8/humidity",0),
		("68:67:25:4F:37:18/humidity",0),
		("68:67:25:4D:9C:E8/humidity",0),
		("58:CF:79:D9:BE:34/humidity",0),
		("68:67:25:50:05:88/brightness",0),
		("58:CF:79:D9:65:F8/brightness",0),
		("68:67:25:4F:37:18/brightness",0),
		("68:67:25:4D:9C:E8/brightness",0),
		("58:CF:79:D9:BE:34/brightness",0),
		("68:67:25:50:05:88/dewpoint",0),
		("58:CF:79:D9:65:F8/dewpoint",0),
		("68:67:25:4F:37:18/dewpoint",0),
		("68:67:25:4D:9C:E8/dewpoint",0),
		("58:CF:79:D9:BE:34/dewpoint",0),]

Last_T = {"68:67:25:50:05:88": 0, "68:67:25:4F:37:18": 0, "fresh": False}
Last_phi = {"68:67:25:50:05:88": 0, "68:67:25:4F:37:18": 0, "fresh": False}
Last_DP = {"68:67:25:50:05:88": 0,  "68:67:25:4F:37:18": 0, "fresh": False}
Last_L = {"68:67:25:50:05:88": 0,  "68:67:25:4F:37:18": 0, "fresh": False}


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        global Connected                #Use global variable
        Connected = True                #Signal connection
    else:
        print("Connection failed")

def on_message(client, userdata, message):
    data = message.payload
    sender, topic = message.topic.split("/")
    esp = ""

    if (sender=="68:67:25:50:05:88"):
        esp = "esptest"
    elif (sender == "68:67:25:4F:37:18"):
        esp = "esp1"
    elif (sender == "58:CF:79:D9:65:F8"):
        esp = "esp2"
    elif (sender == "68:67:25:4D:9C:E8"):
        esp = "esp3"
    elif (sender == "58:CF:79:D9:BE:34"):
        esp = "esp4"
    else:
        return 0


    print("Just received data from "+ sender + " about "+ topic)

    if(data == b'nan'):
        if(topic == "temperature"):
            Last_T[sender]=data
            Last_T["fresh"]=True
        elif (topic == "humidity"):
            Last_phi[sender] = data
            Last_phi["fresh"]=True
        elif (topic == "brightness"):
            Last_L[sender] = data
            Last_L["fresh"]=True
        elif (topic == "dewpoint"):
            Last_DP[sender] = data
            Last_DP["fresh"]=True
        else:
            return 1
    else:
        Last_T["fresh"]=False
        Last_phi["fresh"]=False
        Last_L["fresh"]=False
        Last_DP["fresh"]=False


    if ((Last_T["fresh"] == True) and (Last_phi["fresh"] == True) and (Last_L["fresh"] == True) and (Last_DP["fresh"] == True)):
        try:
            print(f"INSERT INTO {esp} VALUES('{datetime.datetime.now().strftime('%I:%M%p %d %B, %Y')}', {float(Last_T[sender])}, {float(Last_phi[sender])}, {float(Last_DP[sender])}, {float(Last_L[sender])}")
            cursor.execute(f"INSERT INTO {esp} VALUES('{datetime.datetime.now().strftime('%I:%M%p %d %B, %Y')}', {float(Last_T[sender])}, {float(Last_phi[sender])}, {float(Last_DP[sender])}, {float(Last_L[sender])})")
            conn.commit()
        except BlockingIOError as e:
            print("ERROR :",e,"\nThe program is still working.")
        Last_T["fresh"]=False
        Last_phi["fresh"]=False
        Last_DP["fresh"]=False
        Last_L["fresh"]=False

    return 0

def create_connection(db_file):
    conn = None
    try:
        conn=sqlite3.connect(db_file, check_same_thread=False)
    except Error as e:
        print(e)
    return conn

conn = create_connection("cropmonitoring.db")
cursor = conn.cursor()

Connected = False   #global variable for the state of the connection

client = mqtt.Client("Python")               #create new instance
client.on_connect= on_connect                      #attach function to callback
client.on_message= on_message                      #attach function to callback
client.connect(MQTT_BROKER,MQTT_PORT)              #connect to broker

client.loop_start()        #start the loop

client.subscribe(MQTT_TOPIC)

while(1):
    time.sleep(.1)

